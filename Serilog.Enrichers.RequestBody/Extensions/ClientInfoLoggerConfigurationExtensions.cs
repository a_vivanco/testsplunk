﻿using Serilog.Configuration;
using Serilog.Enrichers.RequestInfo.Enrichers;
using System;

namespace Serilog.Enrichers.RequestInfo.Extensions
{
    public static class ClientInfoLoggerConfigurationExtensions
    {
        public static LoggerConfiguration WithRequestBody(this LoggerEnrichmentConfiguration enrichmentConfiguration)
        {
            if (enrichmentConfiguration == null)
                throw new ArgumentNullException(nameof(enrichmentConfiguration));

            return enrichmentConfiguration.With<RequestBodyEnricher>();
        }

        public static LoggerConfiguration WithResponseBody(this LoggerEnrichmentConfiguration enrichmentConfiguration)
        {
            if (enrichmentConfiguration == null)
                throw new ArgumentNullException(nameof(enrichmentConfiguration));

            return enrichmentConfiguration.With<ResponseBodyEnricher>();
        }
    }
}
