﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System;
using System.IO;
using System.Text;

namespace Serilog.Enrichers.RequestInfo.Enrichers
{
    public class RequestBodyEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public RequestBodyEnricher() : this(new HttpContextAccessor()) { }

        public RequestBodyEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (_httpContextAccessor.HttpContext == null)
                return;

            var request = _httpContextAccessor.HttpContext.Request;
            request.EnableBuffering();

            if (request?.Body == null || !request.Body.CanRead) return;

            try
            {
                var memoryStream = new MemoryStream();
                await request?.Body.CopyToAsync(memoryStream);
                request.Body.Seek(0, SeekOrigin.Begin);

                using (StreamReader reader = new StreamReader(
                        memoryStream,
                        Encoding.UTF8,
                        detectEncodingFromByteOrderMarks: false,
                        bufferSize: 1024,
                        leaveOpen: true))
                {
                    string strRequestBody = await reader.ReadToEndAsync();
                    if (!string.IsNullOrEmpty(strRequestBody))
                    {
                        var requestBodyProperty = new LogEventProperty("RequestBody", new ScalarValue(strRequestBody));
                        logEvent.AddPropertyIfAbsent(requestBodyProperty);
                    }
                }
            }
            catch (ObjectDisposedException) { }

        }
    }
}
