﻿using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;
using System;
using System.Buffers;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Serilog.Enrichers.RequestInfo.Enrichers
{
    public class ResponseBodyEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ResponseBodyEnricher() : this(new HttpContextAccessor()) { }

        public ResponseBodyEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (_httpContextAccessor.HttpContext == null)
                return;
            var response = _httpContextAccessor.HttpContext.Response;
            
            if (response?.Body == null/* || !response.Body.CanRead*/) return;

            await GetListOfStringsFromStreamMoreEfficient(response.Body);

            //try
            //{

            //    response.Body.ReadAsync()
            //    var memoryStream = new MemoryStream();
            //    await response?.Body.CopyToAsync(memoryStream);
            //    response.Body.Seek(0, SeekOrigin.Begin);

            //    using (StreamReader reader = new StreamReader(
            //            response.Body,
            //            Encoding.UTF8,
            //            detectEncodingFromByteOrderMarks: false,
            //            bufferSize: 1024,
            //            leaveOpen: true))
            //    {
            //        string strResponseBody = await reader.ReadToEndAsync();
            //        if (!string.IsNullOrEmpty(strResponseBody))
            //        {
            //            var responseBodyProperty = new LogEventProperty("ResponseBody", new ScalarValue(strResponseBody));
            //            logEvent.AddPropertyIfAbsent(responseBodyProperty);
            //        }
            //    }
            //}
            //catch (ObjectDisposedException) { }

        }

        private async Task<List<string>> GetListOfStringsFromStreamMoreEfficient(Stream requestBody)
        {
            StringBuilder builder = new StringBuilder();
            byte[] buffer = ArrayPool<byte>.Shared.Rent(4096);
            List<string> results = new List<string>();

            while (true)
            {
                var bytesRemaining = await requestBody.ReadAsync(buffer, offset: 0, buffer.Length);

                if (bytesRemaining == 0)
                {
                    results.Add(builder.ToString());
                    break;
                }

                // Instead of adding the entire buffer into the StringBuilder
                // only add the remainder after the last \n in the array.
                var prevIndex = 0;
                int index;
                while (true)
                {
                    index = Array.IndexOf(buffer, (byte)'\n', prevIndex);
                    if (index == -1)
                    {
                        break;
                    }

                    var encodedString = Encoding.UTF8.GetString(buffer, prevIndex, index - prevIndex);

                    if (builder.Length > 0)
                    {
                        // If there was a remainder in the string buffer, include it in the next string.
                        results.Add(builder.Append(encodedString).ToString());
                        builder.Clear();
                    }
                    else
                    {
                        results.Add(encodedString);
                    }

                    // Skip past last \n
                    prevIndex = index + 1;
                }

                var remainingString = Encoding.UTF8.GetString(buffer, prevIndex, bytesRemaining - prevIndex);
                builder.Append(remainingString);
            }

            ArrayPool<byte>.Shared.Return(buffer);

            return results;
        }
    }
}
