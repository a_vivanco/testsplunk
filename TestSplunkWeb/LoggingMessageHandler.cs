﻿using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TestSplunkWeb
{
    internal class LoggingMessageHandler : DelegatingHandler
    {
        private readonly ILogger<LoggingMessageHandler> _logger;

        public LoggingMessageHandler(ILogger<LoggingMessageHandler> logger) : base(new HttpClientHandler())
        {
            _logger = logger;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var requestBody = request.Content?.ReadAsStringAsync();
            var sw = new Stopwatch();
            sw.Start();

            var response = await base.SendAsync(request, cancellationToken);
            var responseBody = await response.Content.ReadAsStringAsync();
            sw.Stop();

            using var scope = _logger.BeginScope(new Dictionary<string, object>
            {
                { "LogType", "Dependency"},
                { "ElapsedMilliseconds", sw.Elapsed.TotalMilliseconds},
                { "HttpMethod", request.Method},
                { "RequestProperties", request.Properties},
                { "RequestBody", requestBody },
                { "ResponseBody", responseBody }
            });

            _logger.LogInformation("End processing HTTP request to {RequestUri} after {ElapsedMilliseconds}ms", request.RequestUri, sw.Elapsed.TotalMilliseconds);

            return response;
        }
    }
}