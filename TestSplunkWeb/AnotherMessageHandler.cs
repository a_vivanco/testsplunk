﻿using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace TestSplunkWeb
{
    internal class AnotherMessageHandler : DelegatingHandler
    {
        private readonly ILogger<AnotherMessageHandler> _logger;

        public AnotherMessageHandler(ILogger<AnotherMessageHandler> logger)
        {
            _logger = logger;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            _logger.LogInformation("EEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");
            return await base.SendAsync(request, cancellationToken);
        }
    }
}