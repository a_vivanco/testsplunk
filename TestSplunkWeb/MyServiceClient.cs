﻿using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Threading.Tasks;

namespace TestSplunkWeb
{
    public class MyServiceClient
    {
        private readonly HttpClient _client;
        private readonly ILogger<MyServiceClient> _logger;

        public MyServiceClient(HttpClient client, ILogger<MyServiceClient> logger)
        {
            _client = client;
            _logger = logger;
        }

        public async Task GetGoogle()
        {
            var response = await _client.GetAsync("http://www.google.es");
            if (response.IsSuccessStatusCode)
            {
                var r = await response.Content.ReadAsStringAsync();
                _logger.LogInformation($"Response is OK!");
            }
        }
    }
}