﻿using Lidl.Library.Logging.Splunk.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace Lidl.Library.Logging.Splunk.Internal
{
    internal class CustomLoggingScopeHttpMessageHandler : DelegatingHandler
    {
        private readonly ILogger _logger;
        private readonly HttpClientLoggingOptions _options;

        public CustomLoggingScopeHttpMessageHandler(ILogger logger, HttpClientLoggingOptions options)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
            _options = options;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            if (request == null)
            {
                throw new ArgumentNullException(nameof(request));
            }

            var requestBody = _options.EnabledBodyLogging ? (request.Content != null ? await request.Content.ReadAsStringAsync() : null) : null;
            var requestHeaders = _options.EnabledHeadersLogging ? request.Headers : null;

            using (_logger.BeginScope(new Dictionary<string, object>
                {
                    { "LogType", "Dependency"},
                    { "HttpMethod", request.Method},
                    { "RequestBody", requestBody },
                    { "RequestHeaders", requestHeaders }
                }))
            {
                _logger.LogInformation(new EventId(100, "RequestPipelineStart"), "Start processing HTTP request {HttpMethod} {Uri}", request.Method, request.RequestUri);
                Stopwatch stopwatch = new Stopwatch();
                stopwatch.Start();
                var response = await base.SendAsync(request, cancellationToken);

                var responseBody = _options.EnabledBodyLogging ? await response.Content.ReadAsStringAsync() : null;
                var responseHeaders = _options.EnabledHeadersLogging ? response.Headers.ToDictionary(x => x.Key, x => x.Value) : null;

                using (_logger.BeginScope(new Dictionary<string, object>
                {
                    { "ElapsedMilliseconds", stopwatch.Elapsed.TotalMilliseconds},
                    { "ResponseBody", responseBody },
                    { "@ResponseHeaders", responseHeaders }
                }))
                {
                    _logger.LogInformation(new EventId(101, "RequestPipelineEnd"), "End processing HTTP request {HttpMethod} {Uri} {StatusCode}", request.Method, request.RequestUri, response.StatusCode);
                }

                return response;
            }
        }
    }
}