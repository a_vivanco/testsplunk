﻿using Lidl.Library.Logging.Splunk.Configuration;
using Microsoft.Extensions.Http;
using Microsoft.Extensions.Http.Logging;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Linq;

namespace Lidl.Library.Logging.Splunk.Internal
{
    public class CustomLoggerHttpMessageHandlerBuilderFilter : IHttpMessageHandlerBuilderFilter
    {
        private readonly ILoggerFactory _loggerFactory;
        private readonly IOptionsMonitor<HttpClientLoggingOptions> _httpClientLoggingOptions;

        public CustomLoggerHttpMessageHandlerBuilderFilter(ILoggerFactory loggerFactory, IOptionsMonitor<HttpClientLoggingOptions> httpClientLoggingOptions)
        {
            _loggerFactory = loggerFactory ?? throw new ArgumentNullException(nameof(loggerFactory));
            _httpClientLoggingOptions = httpClientLoggingOptions;
        }

        public Action<HttpMessageHandlerBuilder> Configure(Action<HttpMessageHandlerBuilder> next)
        {
            if (next == null)
            {
                throw new ArgumentNullException(nameof(next));
            }

            return (builder) =>
            {
                // Run other configuration first, we want to decorate.
                next(builder);

                var loggerName = !string.IsNullOrEmpty(builder.Name) ? builder.Name : "Default";

                // We want all of our logging message to show up as-if they are coming from HttpClient,
                // but also to include the name of the client for more fine-grained control.
                var outerLogger = _loggerFactory.CreateLogger($"System.Net.Http.HttpClient.{loggerName}.LogicalHandler");

                var extraHandlers =
                    builder.AdditionalHandlers
                                .Where(x => x.GetType() != typeof(LoggingScopeHttpMessageHandler) &&
                                            x.GetType() != typeof(LoggingHttpMessageHandler)).ToList();

                builder.AdditionalHandlers.Clear();

                foreach (var extraHandler in extraHandlers)
                {
                    builder.AdditionalHandlers.Add(extraHandler);
                }

                var value = _httpClientLoggingOptions.Get(builder.Name);
                builder.AdditionalHandlers.Insert(0, new CustomLoggingScopeHttpMessageHandler(outerLogger, value ?? new HttpClientLoggingOptions()));
            };
        }
    }
}
