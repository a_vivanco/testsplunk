﻿namespace Lidl.Library.Logging.Splunk.Configuration
{
    public class HttpClientLoggingOptions
    {
        public bool EnabledBodyLogging { get; set; }
        public bool EnabledHeadersLogging { get; set; }
    }
}