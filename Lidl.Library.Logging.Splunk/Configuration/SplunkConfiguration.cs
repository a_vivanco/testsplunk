﻿namespace Lidl.Library.Logging.Splunk.Configuration
{
    public class SplunkConfiguration
    {
        public string Address { get; set; }
        public string Token { get; set; }
    }
}
