﻿using Lidl.Library.Logging.Splunk.Extensions;
using Microsoft.AspNetCore.Http;
using Serilog.Core;
using Serilog.Events;

namespace Lidl.Library.Logging.Splunk.Enrichers
{
    public class ClientInfoEnricher : ILogEventEnricher
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        
        public ClientInfoEnricher() : this(new HttpContextAccessor()) { }

        public ClientInfoEnricher(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (!(_httpContextAccessor?.HttpContext?.Request is HttpRequest request)) return;

            var ip = request.GetIpBehindGateway();
            var userAgent = request.GetHeaderValue("User-Agent");
            var originalRequest = request.GetUriBehindGateway();

            var ipProperty = propertyFactory.CreateProperty("ClientIp", ip.ToString());
            var userAgentProperty = propertyFactory.CreateProperty("ClientAgent", userAgent);
            var originalRequestProperty = propertyFactory.CreateProperty("OriginalRequest", originalRequest.AbsoluteUri);

            logEvent.AddPropertyIfAbsent(ipProperty);
            logEvent.AddPropertyIfAbsent(userAgentProperty);
            logEvent.AddPropertyIfAbsent(originalRequestProperty);
        }
    }
}
