﻿using Serilog.Core;
using Serilog.Events;

namespace Lidl.Library.Logging.Splunk.Enrichers
{
    public class LogLevelCompactEnricher : ILogEventEnricher
    {
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty("Level", logEvent.Level.ToString()));
        }
    }
}
