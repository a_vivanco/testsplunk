﻿using Lidl.Library.Logging.Splunk.Configuration;
using Lidl.Library.Logging.Splunk.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Http;
using System;
using System.Collections.Generic;
using System.Text;

namespace Lidl.Library.Logging.Splunk.Extensions
{
    public static class HttpClientBuilderExtensions
    {
        public static IHttpClientBuilder EnableBodyLogging(this IHttpClientBuilder builder)
        {
            builder.Services.Replace(ServiceDescriptor.Singleton<IHttpMessageHandlerBuilderFilter, CustomLoggerHttpMessageHandlerBuilderFilter>());
            builder.Services.PostConfigure<HttpClientLoggingOptions>(builder.Name, x => x.EnabledBodyLogging = true);
            return builder;
        }

        public static IHttpClientBuilder EnableHeadersLogging(this IHttpClientBuilder builder)
        {
            builder.Services.Replace(ServiceDescriptor.Singleton<IHttpMessageHandlerBuilderFilter, CustomLoggerHttpMessageHandlerBuilderFilter>());
            builder.Services.PostConfigure<HttpClientLoggingOptions>(builder.Name, x => x.EnabledHeadersLogging = true);
            return builder;
        }
    }
}
