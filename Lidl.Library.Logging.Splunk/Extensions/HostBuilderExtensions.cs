﻿using Lidl.Library.Logging.Splunk.Configuration;
using Lidl.Library.Logging.Splunk.Enrichers;
using Lidl.Library.Logging.Splunk.Formatters;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Sinks.Splunk;
using System;
using System.Net.Http;

namespace Lidl.Library.Logging.Splunk.Extensions
{
    public static class HostBuilderExtensions
    {
        public static IHostBuilder AddSplunk(this IHostBuilder builder, Action<IServiceProvider, SplunkConfiguration> configureSplunk)
        {
            return builder.UseSerilog((HostBuilderContext context, IServiceProvider serviceProvider, LoggerConfiguration configuration) =>
            {
                var splunkConfiguration = new SplunkConfiguration();
                configureSplunk(serviceProvider, splunkConfiguration);

                var config = serviceProvider.GetRequiredService<IConfiguration>();

            configuration
                .Enrich.FromLogContext()
                .Enrich.WithProperty("Application", "TestSplunk")
                .Enrich.With<ClientInfoEnricher>()
                .Enrich.With<LogLevelCompactEnricher>()
#if DEBUG
                    .WriteTo.Debug()
#endif
                    .WriteTo.EventCollector(splunkConfiguration.Address, splunkConfiguration.Token, new CustomMessageFormatter(), messageHandler: new HttpClientHandler()
                    {
                        ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
                    });
            });
        }
        
        public static IHostBuilder AddSplunk(this IHostBuilder builder)
        {
            return builder.AddSplunk((IServiceProvider serviceProvider, SplunkConfiguration splunkConfiguration) =>
            {
                var config = serviceProvider.GetRequiredService<IConfiguration>();
                splunkConfiguration.Address = config.GetValue<string>("Splunk:Address");
                splunkConfiguration.Token = config.GetValue<string>("Splunk:Token");
            });
        }
    }
}
