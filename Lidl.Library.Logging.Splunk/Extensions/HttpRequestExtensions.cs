﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Lidl.Library.Logging.Splunk.Extensions
{
    public static class HttpRequestExtensions
    {
        private const string ForwardedScheme = "x-forwarded-proto";
        private const string OriginalHost = "x-original-host";
        private const string ForwardedHost = "x-forwarded-host";
        private const string ForwardedPort = "x-forwarded-port";
        private const string ForwardedFor = "x-forwarded-for";

        public static Uri GetUriBehindGateway(this HttpRequest request)
        {
            var scheme = request.GetHeaderValue(ForwardedScheme) ?? request.Scheme;
            var host = request.GetHeaderValue(OriginalHost) ?? GetHeaderValue(request, ForwardedHost) ?? request.Host.Host;
            var port = request.GetHeaderValue(ForwardedPort) ?? (request.Host.Port.HasValue ? request.Host.Port.Value.ToString() : null);

            var builder = new UriBuilder();
            builder.Scheme = scheme;
            builder.Host = host;
            builder.Path = request.Path;
            builder.Query = request.QueryString.ToUriComponent();

            if (!string.IsNullOrEmpty(port) && int.TryParse(port, out var p))
            {
                builder.Port = p;
            }

            return builder.Uri;
        }

        public static IPAddress GetIpBehindGateway(this HttpRequest request)
        {
            var defaultIp = request.HttpContext.Connection.RemoteIpAddress;
            var forwardedIps = request.GetHeaderValue(ForwardedFor);
            if (string.IsNullOrWhiteSpace(forwardedIps))
                return defaultIp;

            var forwardedIpList = forwardedIps.Split(',');
            if (forwardedIpList.Length <= 0)
                return defaultIp;

            var forwardedIpIndex = Math.Max(forwardedIpList.Length - 2, 0);
            var forwardedIp = forwardedIpList[forwardedIpIndex];
            var ip = forwardedIp.Split(':')[0].Trim();
            if (!IPAddress.TryParse(ip, out var result))
                return defaultIp;

            return result;
        }

        public static string GetHeaderValue(this HttpRequest request, string name, string defaultValue = null)
        {
            var result = request.Headers[name].ToString();
            if (string.IsNullOrEmpty(result))
            {
                return defaultValue;
            }

            return result;
        }
    }
}
