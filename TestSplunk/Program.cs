﻿using Serilog;
using System;
using System.Net.Http;
using System.Threading;

namespace TestSplunk
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var log = new LoggerConfiguration()
                .WriteTo.EventCollector("https://scrm-splunk-poc.westeurope.cloudapp.azure.com:8080/services/collector/event", "933ee9df-f97e-49df-8440-e06ddc85d968", messageHandler: new HttpClientHandler()
                {
                    ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => { return true; }
                })

                .CreateLogger())
                {
                    var count = 1000;
                    while (count < 10000)
                    {
                        log.Error($"This is an error {++count}");
                    }
                }

            Thread.Sleep(5000);
        }
    }
}
